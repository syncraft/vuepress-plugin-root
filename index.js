module.exports = ({ target }) => {
  return {
    extendPageData(page) {
      if (target && page.path == target || page.path == `${target}/`) {
        page.path = '/'
        page.regularPath = '/'
      }
    }
  }
}